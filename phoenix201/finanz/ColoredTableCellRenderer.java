package phoenix201.finanz;
import java.awt.Color;
import java.awt.Component;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ColoredTableCellRenderer extends DefaultTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7272725735288258640L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col){
	
		this.setText(value.toString());
		
		if (col == 3){
			double d = Double.parseDouble(value.toString());
			if (d < 0)
				this.setBackground(Color.red);
			String s = NumberFormat.getCurrencyInstance(Locale.GERMANY).format(d);
			this.setText(s);
		}
		else 
			this.setBackground(Color.white);
		
		return this;
	}
}
