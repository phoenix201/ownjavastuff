package phoenix201.finanz;
import java.io.File;

import phoenix201.me.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class WriteXML {
	
	public WriteXML(){
		
	}
	
	public WriteXML(ArrayList pL){
		writeXML(pL);
	}

	public void writeXML(ArrayList postenListe){

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("abrechnung");
			doc.appendChild(rootElement);
			
			for (int i = 0; i < postenListe.size(); i++) {
				
				Posten p = (Posten)postenListe.getElement(i);
				Element posten = doc.createElement("posten");
				rootElement.appendChild(posten);

				Element datum = doc.createElement("datum");
				datum.setTextContent(p.getDatum());
				posten.appendChild(datum);

				Element betreff = doc.createElement("betreff");
				betreff.setTextContent(p.getBetreff());
				posten.appendChild(betreff);

				Element betrag = doc.createElement("betrag");
				betrag.setTextContent("" + p.getBetrag());
				posten.appendChild(betrag);

				Element kategorie = doc.createElement("kategorie");
				kategorie.setTextContent("" + p.getKategorie());
				posten.appendChild(kategorie);

			}

			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("test.xml"));
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);
			System.out.println("File saved");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();

		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
}
