package phoenix201.finanz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.text.NumberFormat;

import phoenix201.me.ArrayList;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


public class FinanzenGUI {

	private JFrame frame1;
	private JTable table1;
	private JLabel label1;
	private JPanel saldoGes;
	private JLabel saldo1;
	private JLabel saldo2;
	private JLabel saldo3;
	private JLabel saldo4;
	private FinanzenTableModel model = new FinanzenTableModel();
	private DefaultTableCellRenderer ren = new ColoredTableCellRenderer();
	private ArrayList postenListe = Posten.getPostenListe();
	private double saldo = 0;

	public FinanzenGUI() {

		this.initWindow();
	}

	public void initWindow() {

		String[] columnNames = { "Datum", "Kat", "Betreff", "Betrag" };

		table1 = new JTable(model);
		//table1.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table1.setFillsViewportHeight(true);
		

		model.setColumnNames(columnNames);
		model.setContent(createTableContent());
		model.fireTableStructureChanged();
		
//		table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//		int zehntel = table1.getSize().width/10;
//		table1.getColumnModel().getColumn(0).setPreferredWidth(2*zehntel);
//		table1.getColumnModel().getColumn(1).setPreferredWidth(zehntel);
//		table1.getColumnModel().getColumn(3).setPreferredWidth(2*zehntel);
		table1.setDefaultRenderer(Object.class, ren);
		
		saldoGes = new JPanel();
		saldoGes.setLayout(new BoxLayout(saldoGes, BoxLayout.Y_AXIS));

		label1 = new JLabel();
		if (saldo < 0)
			label1.setForeground(Color.red);
		label1.setText("Aktueller Saldo: " + NumberFormat.getCurrencyInstance(Locale.GERMANY).format(saldo));
		
		saldo1 = new JLabel();
		saldo1.setText("Essen: " + NumberFormat.getCurrencyInstance(Locale.GERMANY).format(Posten.getSaldo1()));

		saldo2 = new JLabel();
		saldo2.setText("Einrichtung: " + NumberFormat.getCurrencyInstance(Locale.GERMANY).format(Posten.getSaldo2()));

		saldo3 = new JLabel();
		saldo3.setText("Miete/Behörden: " + NumberFormat.getCurrencyInstance(Locale.GERMANY).format(Posten.getSaldo3()));
		
		saldo4 = new JLabel();
		saldo4.setText("Spaß: " + NumberFormat.getCurrencyInstance(Locale.GERMANY).format(Posten.getSaldo4()));
		
//		saldoGes.add(label1, BorderLayout.CENTER);
//		saldoGes.add(saldo1, BorderLayout.PAGE_END);
//		saldoGes.add(saldo2, BorderLayout.PAGE_END);
//		saldoGes.add(saldo3, BorderLayout.PAGE_END);
//		saldoGes.add(saldo4, BorderLayout.PAGE_END);
		
		
		frame1 = new JFrame();
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setSize(800, 600);

		//frame1.setLayout(new BorderLayout());
		frame1.getContentPane().add(table1, BorderLayout.CENTER);
		frame1.getContentPane().add(new JScrollPane(table1));
		frame1.getContentPane().add(saldoGes, BorderLayout.PAGE_END);
		saldoGes.add(label1);
		saldoGes.add(saldo1);
		saldoGes.add(saldo2);
		saldoGes.add(saldo3);
		saldoGes.add(saldo4);

		// frame1.getContentPane().add(cp);
		//frame1.setMinimumSize(new Dimension(800, 600));
		frame1.pack();
		frame1.setVisible(true);
	}

	public Object[][] createTableContent() {
		String[][] tableContent = new String[postenListe.size()][6];
		
		for (int i = 0; i < postenListe.size(); i++) {
			Posten p = (Posten)postenListe.getElement(i);
			tableContent[i][0] = p.getDatum();
			tableContent[i][1] = "" + p.getKategorie();
			tableContent[i][2] = p.getBetreff();
			// tableContent[i][3] = "" +
			// NumberFormat.getCurrencyInstance(Locale.GERMANY).format(p.getBetrag());
			tableContent[i][3] = "" + p.getBetrag();
			saldo += p.getBetrag();
			

		}
		return tableContent;
	}

}
