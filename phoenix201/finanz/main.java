package phoenix201.finanz;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import phoenix201.me.ArrayList;
import java.util.Date;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class main {

	private static String filename = "test.xml";
	private static String trennlinie = "+----|----------|---|-------------------------|---------+";
	
	public static void main(String args[]) {

		
		parseXML(filename);
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("-g")) {
				startGUI();
			} else if (args[0].equalsIgnoreCase("--testWrite")) {
				// new WriteXML()
			} else if (args[0].equalsIgnoreCase("--a")) {
				startMenu();
			}
		}

	}

	public static void parseXML(String file) {
		try {
			// XMLReader erzeugen
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();

			// Pfad zur XML Datei
			FileReader reader = new FileReader(file);
			InputSource inputSource = new InputSource(reader);
			// DTD kann optional u:bergeben werden

			xmlReader.setContentHandler(new PostenContentHandler());

			// Parsen wird gestartet
			xmlReader.parse(inputSource);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public static void startGUI() {
		FinanzenGUI gui = new FinanzenGUI();
	}

	public static void startMenu() {
		while (true) {
			System.out.println("Herzlich Wilkommen in der FinanzAnwendung");
			System.out.println("d = display, a = add");
			String inputString = readInputString();
			if (inputString.equalsIgnoreCase("d")) {
				displayPosten();
			} else if (inputString.equalsIgnoreCase("a")) {
				addPosten();
			} else if (inputString.equalsIgnoreCase("q")) {
				System.exit(0);
			} else if (inputString.equalsIgnoreCase("r")) {
				removePosten();
			} else if (inputString.equalsIgnoreCase("w")) {
				writePosten();
			} else if (inputString.equalsIgnoreCase("s")){
				showSaldo();
			}
		}

	}

	public static void displayPosten() {
		
		System.out.println(trennlinie);
		System.out
				.println("+ Nr.|Datum     |Kat|Betreff                  |Betrag   +");
		System.out.println(trennlinie);
		ArrayList apl = Posten.getPostenListe();
		for (int i = 0; i < apl.size();i++) {
			System.out.println(""+i);
			System.out.println(""+apl.size());
			Posten p = (Posten)apl.getElement(i);
			System.out.println("+ " //+ fillToWidth("" + p.getId(), 3) + "|"
					+ fillToWidth(p.getDatum(), 10) + "|"
					+ fillToWidth("" + p.getKategorie(), 3) + "|"
					+ fillToWidth(p.getBetreff(), 25) + "|"
					+ fillToWidth("" + p.getBetrag(), 9) + "+");
			
		}
		System.out.println(trennlinie);
	}

	public static String fillToWidth(String s, int i) {
		while (s.length() < i) {
			s = s + " ";
		}
		return s;
	}

	public static void addPosten() {
		Posten aP = new Posten();
		System.out.println("Datum eingeben");
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		String dateString = dateFormat.format(date);
		String temp = readInputString();
		if (temp.length() > 3) {
			dateString = temp;
		}
		aP.setDatum(dateString);

		System.out.println("Kategorie eingeben");
		try {
			int kat = Integer.parseInt(readInputString());
			aP.setKategorie(kat);
		} catch (Exception e) {
			System.out.println("Falsche Eingabe nutze default 0");
			aP.setKategorie(0);
		}

		System.out.println("Betreff eingeben");
		String betr = readInputString();
		aP.setBetreff(betr);

		System.out.println("Betrag eingeben");
		try {
			double betra = Double.parseDouble(readInputString());
			aP.setBetrag(betra);
		} catch (Exception e) {
			System.out.println("Falsche Eingabe nutze default 0.0");
			aP.setBetrag(0);
		}
		ArrayList aPl = Posten.getPostenListe();
		int index = aPl.size();
		aP.setId(index+1);
		aPl.add(aP);
		Posten.setPostenListe(aPl);
		
	}
	
	public static void removePosten(){
		System.out.println("type in the number of the entry to remove");
		int entry = Integer.parseInt(readInputString());
		ArrayList aPl = Posten.getPostenListe();
		aPl.remove(entry-1);
		Posten.setPostenListe(aPl);
		}
	
	public static void writePosten(){
		new WriteXML().writeXML(Posten.getPostenListe());
	}
	
	public static void showSaldo(){
		System.out.println("Saldi");
		System.out.println("General: " + Posten.getSaldo());
		System.out.println("Essen: " + Posten.getSaldo1());
		System.out.println("Einrichtung: " + Posten.getSaldo2());
		System.out.println("Behörden/Miete: " + Posten.getSaldo3());
		System.out.println("Spaß: " + Posten.getSaldo4());
		
		
	}

	public static String readInputString() {
		String inputString = "";
		// Scanner input = new Scanner(System.in); // Decl. & init. a Scanner.
		//
		// inputString = input.next(); // Get what the user types.
		// //System.out.println();// Move down to a fresh line.
		// input.close();
		
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);
		try {
			inputString = in.readLine();

		} catch (Exception e) {
			System.out.println("wrong input");
		}

		return inputString;
	}
}
