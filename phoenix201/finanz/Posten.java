package phoenix201.finanz;
import java.text.NumberFormat;
import phoenix201.me.ArrayList;
import java.util.Locale;

public class Posten {

	private double betrag = 0.0;
	private String betreff = "";
	/*
	 * @var Diese Variable enthält die Kategorie. 0 = ohne 1 = Essen 2 =
	 * Einrichtung 3 = Behörden/Miete 4 = Spass
	 */
	private int id = 0;
	private int kategorie = 0;
	private String datum;
	private static ArrayList postenListe;
	private static double saldo = 0.0;
	private static double saldo1 = 0.0;
	private static double saldo2 = 0.0;
	private static double saldo3 = 0.0;
	private static double saldo4 = 0.0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public static double getSaldo() {
		return saldo;
	}

	public static void setSaldo(double saldo) {
		Posten.saldo = saldo;
	}

	public static double getSaldo1() {
		return saldo1;
	}

	public static void setSaldo1(double saldo1) {
		Posten.saldo1 = saldo1;
	}

	public static double getSaldo2() {
		return saldo2;
	}

	public static void setSaldo2(double saldo2) {
		Posten.saldo2 = saldo2;
	}

	public static double getSaldo3() {
		return saldo3;
	}

	public static void setSaldo3(double saldo3) {
		Posten.saldo3 = saldo3;
	}

	public static double getSaldo4() {
		return saldo4;
	}

	public static void setSaldo4(double saldo4) {
		Posten.saldo4 = saldo4;
	}

	public static ArrayList getPostenListe() {
		return postenListe;
	}

	public static void setPostenListe(ArrayList postenListe) {
		Posten.postenListe = postenListe;
	}

	public Posten() {

	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public double getBetrag() {
		return betrag;
	}

	public void setBetrag(double betrag) {
		this.betrag = betrag;
	}

	public String getBetreff() {
		return betreff;
	}

	public void setBetreff(String betreff) {
		this.betreff = betreff;
	}

	public int getKategorie() {
		return kategorie;
	}

	public void setKategorie(int kategorie) {
		this.kategorie = kategorie;
	}

	@Override
	public String toString() {
		return "[["
				+ this.id
				+ "] ["
				+ this.datum
				+ "] ["
				+ this.kategorie
				+ "] ["
				+ this.betreff
				+ "]"
				+ " ["
				+ NumberFormat.getCurrencyInstance(Locale.GERMANY).format(
						this.betrag) + " ]]";
	}

}
