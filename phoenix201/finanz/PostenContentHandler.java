package phoenix201.finanz;
import phoenix201.me.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;


public class PostenContentHandler implements ContentHandler {

	private ArrayList allePosten = new ArrayList();
	private String currentValue;
	private Posten posten;
	int i = 1;

	// Aktuelle Zeichen die gelesen werden, werden in eine Zwischenvariable
	// gespeichert
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		currentValue = new String(ch, start, length);
	}

	// Methode wird aufgerufen wenn der Parser zu einem Start-Tag kommt
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (localName.equals("posten")) {
			// Neue Person erzeugen
			posten = new Posten();
			// Attribut id wird in einen Integer umgewandelt und dann zu der
			// jeweiligen Person gesetzt
			// posten.setId(Integer.parseInt(atts.getValue("id")));
		}
	}

	// Methode wird aufgerufen wenn der Parser zu einem End-Tag kommt
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// Datum parsen und setzen
		if (localName.equals("datum")) {
			// SimpleDateFormat datumsformat = new
			// SimpleDateFormat("dd.MM.yyyy");
			// try {
			// Date date = datumsformat.parse(currentValue);
			// posten.setDatum(date);
			// } catch (ParseException e) {
			// e.printStackTrace();
			// }
			posten.setDatum(currentValue);
		}
		// Name setzen
		// if (localName.equals("ausgabe")) {
		// posten.setAusgabe(Boolean.parseBoolean(currentValue));
		// }
		// Vorname setzen
		if (localName.equals("betrag")) {
			posten.setBetrag(Double.parseDouble(currentValue));
		}
		// Datum parsen und setzen
		if (localName.equals("betreff")) {

			posten.setBetreff(currentValue);

		}
		// Postleitzahl setzen
		if (localName.equals("kategorie")) {
			int cvar = Integer.parseInt(currentValue);
			posten.setKategorie(cvar);
			switch (Integer.parseInt(currentValue)) {

			case 1:
				Posten.setSaldo1(Posten.getSaldo1() + posten.getBetrag());
				break;
			case 2:
				Posten.setSaldo2(Posten.getSaldo2() + posten.getBetrag());
				break;
			case 3:
				Posten.setSaldo3(Posten.getSaldo3() + posten.getBetrag());
				break;
			case 4:
				Posten.setSaldo4(Posten.getSaldo4() + posten.getBetrag());
				break;
			default:
				break;
			}
		}
		// Ort setzen
		// if (localName.equals("id")) {
		// posten.setId(Integer.parseInt(currentValue));
		// }
		// Person in Personenliste abspeichern falls Person End-Tag erreicht
		// wurde.
		if (localName.equals("posten")) {
			posten.setId(i);
			i++;
			allePosten.add(posten);
			//Posten.setPostenListe(allePosten);
			System.out.println(posten);
		}
	}

	public void endDocument() throws SAXException {
			Posten.setPostenListe(allePosten);
	}

	public void endPrefixMapping(String prefix) throws SAXException {
	}

	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
	}

	public void processingInstruction(String target, String data)
			throws SAXException {
	}

	public void setDocumentLocator(Locator locator) {
	}

	public void skippedEntity(String name) throws SAXException {
	}

	public void startDocument() throws SAXException {
	}

	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
	}

}
