package phoenix201.finanz;
import javax.swing.table.AbstractTableModel;

public class FinanzenTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6006900434720620140L;

	private boolean DEBUG = true;

	private String[] columnNames = {};
	private Object[][] data = {};

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public boolean isCellEditable(int row, int col) {
		if (col < 2) {
			return false;
		} else {
			return true;
		}
	}

	public void setValueAt(Object value, int row, int col) {
		if (DEBUG) {
			System.out.println("Setting value at " + row + "," + col + " to "
					+ value + " (an instance of " + value.getClass() + ")");
		}

		data[row][col] = value;
		fireTableCellUpdated(row, col);

		if (DEBUG) {
			System.out.println("New value of data:");
			printDebugData();
		}
	}

	public void setColumnNames(String[] names) {
		this.columnNames = names;
	}

	public void setContent(Object[][] data) {
		this.data = data;

	}

	private void printDebugData() {
		int numRows = getRowCount();
		int numCols = getColumnCount();

		for (int i = 0; i < numRows; i++) {
			System.out.println("     row " + i + ":");
			for (int j = 0; j < numCols; j++) {
				System.out.println("    " + data[i][j]);
			}
			System.out.println();
		}
		System.out.println("-----------------------------------------");
	}
}
